# Copyright (C) 2008 Mark Hills <mark@pogo.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2, as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License version 2 for more details.
# 
# You should have received a copy of the GNU General Public License
# version 2 along with this program; if not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

#CFLAGS += -std=gnu99 -Wall -O3 -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include
CFLAGS += -std=gnu99 -Wall -g -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include

SDL_CFLAGS = `sdl-config --cflags`
SDL_LIBS = `sdl-config --libs` -lSDL_ttf
ALSA_LIBS = -lasound
JACK_LIBS = `pkg-config --libs jack`
G_LIBS = -lglib-2.0

# Import the optional configuration

-include .config

# Core objects and libraries

OBJS = interface.o library.o player.o rig.o timecoder.o track.o track_cache.o xwax.o record.o include.o
DEVICE_OBJS = device.o oss.o
DEVICE_CPPFLAGS =
DEVICE_LIBS =

# Optional device types

ifdef ALSA
DEVICE_OBJS += alsa.o
DEVICE_CPPFLAGS += -DWITH_ALSA
DEVICE_LIBS += $(ALSA_LIBS)
endif

ifdef JACK
DEVICE_OBJS += jack.o
DEVICE_CPPFLAGS += -DWITH_JACK
DEVICE_LIBS += $(JACK_LIBS)
endif

# Rules

.PHONY:		clean depend

xwax:		$(OBJS) $(DEVICE_OBJS)
		$(CC) $(CFLAGS) -o $@ $^ -pthread $(SDL_LIBS) $(DEVICE_LIBS) $(G_LIBS)

interface.o:	CFLAGS += $(SDL_CFLAGS)

xwax.o:		CFLAGS += $(DEVICE_CPPFLAGS)

depend:		device.c interface.c library.c player.c rig.c timecoder.c \
		track.c track_cache.c xwax.c record.c include.c
		$(CC) -MM $^ > .depend

clean:
		rm -f .depend xwax *.o *~

-include .depend
