
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>

#include <jack/jack.h>

#include "device.h"
#include "timecoder.h"
#include "player.h"
#include "jack.h"


/* used from device.h
 * #define DEVICE_CHANNELS 2
 * #define DEVICE_RATE 44100
 *
 * TODO check implication of this:
 * #define DEVICE_FRAME 32
 */

#if DEVICE_CHANNELS != 2
#error jack.c only supports DEVICE_CHANNELS == 2
#endif

#define JACKNAME_MAX 64

#define SCALE ((float)(1<<15))

struct jack_local_t {
    int deck_index;

    char client_name[JACKNAME_MAX];
    char server_name[JACKNAME_MAX];

    jack_client_t *client;
    jack_options_t options;
    jack_status_t status;

    jack_port_t *input_port[DEVICE_CHANNELS];
    jack_port_t *output_port[DEVICE_CHANNELS];

	signed short* pcm;

    int xwax_status_started;
};


/* Start/stop the audio device capture and playback */

static int start(struct device_t *dv)
{
    struct jack_local_t *j = (struct jack_local_t *)dv->local;

    j->xwax_status_started = 1;

    return 0;
}

static int stop(struct device_t *dv)
{
    struct jack_local_t *j = (struct jack_local_t *)dv->local;

    j->xwax_status_started = 0;

    return 0;
}


int clear(struct device_t *dv)
{
    struct jack_local_t *j = (struct jack_local_t *)dv->local;

	g_free(j->pcm);

    /* deactivate jack */
    jack_client_close(j->client);

    /* free j */
    free(dv->local);

    return 0;
}

/* Pull audio from jackd */
static int device_pull(struct device_t *dv, signed short *pcm, jack_nframes_t nframes)
{
    struct jack_local_t *j = (struct jack_local_t *)dv->local;

    jack_default_audio_sample_t * in[DEVICE_CHANNELS];
/*    
    jack_transport_state_t ts;
*/    
    int i, n;
/*
    ts = jack_transport_query(j->client, NULL);

    if(ts == JackTransportRolling) {
*/    
        for(i = 0; i < DEVICE_CHANNELS; i++)
            in[i] = jack_port_get_buffer(j->input_port[i], nframes);

        /* fprintf(stderr, "frame0: %f %f\n", SCALE*in[0][0], SCALE*in[1][0]); */

        for(n = 0; n < nframes; n++)
            for(i = 0; i < DEVICE_CHANNELS; i++)
                pcm[n * DEVICE_CHANNELS + i] = (signed short)(SCALE*(float)in[i][n]);
/*  
    }
*/

    return nframes;
}

/* Push audio to jackd */
int device_push(struct device_t *dv, signed short *pcm, jack_nframes_t nframes)
{
    struct jack_local_t *j = (struct jack_local_t *)dv->local;

    jack_default_audio_sample_t * out[DEVICE_CHANNELS];
/*
    jack_transport_state_t ts;
*/
    int i, n;
/*
    ts = jack_transport_query(j->client, NULL);

    if(ts == JackTransportRolling) {
*/
        for(i = 0; i < DEVICE_CHANNELS; i++)
            out[i] = jack_port_get_buffer(j->output_port[i], nframes);

        for(n = 0; n < nframes; n++)
            for(i = 0; i < DEVICE_CHANNELS; i++)
                out[i][n] = (float)pcm[n * DEVICE_CHANNELS + i] * (1.f / (float)(SCALE));
/*
    }
*/

    return nframes;
}

/*
 * The process callback for this JACK application is called in a
 * special realtime thread once for each audio cycle:
 */
static int jack_callback(jack_nframes_t nframes, void *arg)
{
    struct device_t *dv = (struct device_t *)arg;
    struct jack_local_t *j = (struct jack_local_t *)dv->local;

/*
    jack_transport_state_t ts;
*/

    signed short *pcm = j->pcm;
    int samples;

/*
    ts = jack_transport_query(j->client, NULL);
    switch(ts) {
    case JackTransportRolling:
*/

		if(! j->xwax_status_started) {
			/* jack is rolling, but not xwax... */
			return 0;
        	}

		/* Check input buffer for recording */
		samples = device_pull(dv, pcm, nframes);
		if(samples == -1)
			return -1;

		if(dv->timecoder) {
			timecoder_submit(dv->timecoder, pcm, samples);
		}

		/* Always push some audio to the soundcard, even if it means
		 * silence. This has shown itself to be much more reliable
		 * than constantly starting and stopping -- which can affect
		 * other devices to the one which is doing the stopping. */

        	/* Check the output buffer for playback */
        if(dv->player) {
			player_collect(dv->player, pcm, nframes);
		}
		else
			memset(pcm, 0, DEVICE_FRAME * DEVICE_CHANNELS * sizeof(signed short));

		samples = device_push(dv, pcm, nframes);

		if(samples == -1)
			return -1;

		/*break;

    case JackTransportStopped:
        //fprintf(stderr, "jack state: stopped\n");
        break;

    default:
        fprintf(stderr, "jack state: unknown\n");
    }*/

    return 0;
}

/*
 * JACK calls this shutdown_callback if the server ever shuts down or
 * decides to disconnect the client.
 */
static void jack_shutdown(void *arg)
{
    /*
    struct device_t *dv = (struct device_t *)arg;
    struct jack_local_t *j = (struct jack_local_t *)dv->local;
    */

    fprintf(stderr, "jack_shutdown() called. aborting.");
    exit (1);
}

//Every time jack changes the buffer size this function is called
//and we have to make sure the size of the buffer corresponds
static int jack_buffer_size_change(jack_nframes_t nframes, void *arg)
{
	struct device_t *dv = (struct device_t *)arg;
	struct jack_local_t *j = (struct jack_local_t *)dv->local;

	if(nframes < DEVICE_FRAME) {
		fprintf(stderr, "nframes < DEVICE_FRAME: %d < %d\n", nframes, DEVICE_FRAME);
		fprintf(stderr, "to prevent a nice coredump lets abort now...");
		exit(1);
	}
	
	//reallocate with new size 
	j->pcm = g_renew(signed short, j->pcm, nframes * DEVICE_CHANNELS);
	
	if(j->pcm == NULL) {
		perror("malloc");
		exit(1);
	}

	return 0;	
}

/*
 * set up a per-deck jack client
 */
int jack_init(struct device_t *dv, int deck_index)
{
    struct jack_local_t *j;

    char *c;
    int i;
    char name[JACKNAME_MAX];
    jack_port_t *p;

    /* set up dv */
    dv->pollfds = NULL;
    dv->handle = NULL;
    dv->start = start;
    dv->stop = stop;
    dv->clear = clear;

    /* set up j and attach to dv */
    j = malloc(sizeof(struct jack_local_t));
    if(!j) {
        perror("malloc");
        return -1;
    }
    bzero(j, sizeof(struct jack_local_t));
    dv->local = j;

	j->pcm = NULL;
    j->deck_index = deck_index;
    j->options = JackNullOption;

    snprintf(j->client_name, JACKNAME_MAX, "xwax_deck_%u", j->deck_index);

    /* open a client connection to the JACK server */
    j->client = jack_client_open(j->client_name, j->options, &j->status, &j->server_name);
    if(j->client == NULL) {
        fprintf(stderr, "jack_client_open() failed: 0x%02x\n", j->status);

        if(j->status & JackServerFailed)
            fprintf(stderr, "Unable to connect to JACK server\n");

        exit (1);
    }
    if(j->status & JackServerStarted) {
        fprintf (stderr, "JACK server started\n");
    }
    if(j->status & JackNameNotUnique) {
        c = jack_get_client_name(j->client);
        strncpy(j->client_name, c, JACKNAME_MAX);
        fprintf(stderr, "unique name assigned by jackd!\n");
    }
    fprintf(stderr, "jack client name: '%s'\n", j->client_name);

    /* check sample rate */
    i = jack_get_sample_rate(j->client);
    if(i != DEVICE_RATE) {
        fprintf(stderr, "jackd sample rate is %dHz, not %dHz! aborting.\n", i, DEVICE_RATE);
        exit(1);
    }

	//make sure we are notified when the buffer size changes
	jack_set_buffer_size_callback(j->client, jack_buffer_size_change, dv);
	
	//Allocates necessary buffers
	//corresponding to jack's buffer size
	jack_buffer_size_change(jack_get_buffer_size(j->client), dv);			

    /* tell the JACK server to call `jack_callback()' whenever
     * there is work to be done.
     */
    jack_set_process_callback(j->client, jack_callback, dv);

    /* tell the JACK server to call `jack_shutdown()' if
     * it ever shuts down, either entirely, or if it
     * just decides to stop calling us.
     */
    jack_on_shutdown(j->client, jack_shutdown, dv);


    /* create jack ports for this device */
    for(i = 0; i < DEVICE_CHANNELS; i++) {
        snprintf(name, JACKNAME_MAX, "timecode_%d", i);
        fprintf(stderr, "Creating JACK port %s\n", name);
        p = jack_port_register(j->client, name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
        if(p == NULL) {
            fprintf(stderr, "no more JACK ports available\n");
            return -1;
        }
        j->input_port[i] = p;
    }
    for(i = 0; i < DEVICE_CHANNELS; i++) {
        snprintf(name, JACKNAME_MAX, "output_%d", i);
        fprintf(stderr, "Creating JACK port %s\n", name);
        p = jack_port_register (j->client, name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
        if(p == NULL) {
            fprintf(stderr, "no more JACK ports available\n");
            return -1;
        }
        j->output_port[i] = p;
    }

    /* Tell the JACK server that we are ready to roll.  Our
     * jack_callback() will start running now.
     */
    if(jack_activate(j->client)) {
        fprintf(stderr, "cannot activate client");
        exit (1);
    }

    return 0;
}

static gboolean parse_jack_device(const gchar *option_name, const gchar *value, gpointer data, GError **error)
{
	struct device_t* device = createDevice(error);

	if(!device)
		return FALSE;

	return connectAudio(device, jack_init(device, get_device_count()));
}

static GOptionEntry jackOptions[] =
{
	{ "jack_port", 'j', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_NONE, &parse_jack_device, "Build a deck, representing a jack port", NULL},
	{ NULL }
};

GOptionGroup* get_jack_option_group()
{
	GOptionGroup* group = g_option_group_new
	(
		"jack",
		"JACK options",
		"Show JACK help options",
		NULL,
		NULL
	);
	g_option_group_add_entries(group, jackOptions);
	return group;
}

