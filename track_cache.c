#include <stdlib.h>
#include <unistd.h> 
#include <glib/gstdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "include.h"
#include "track.h"
#include "track_cache.h"

static const char* cache_directory = NULL;
static const char cache_file_temp_ending[] = ".temp";
#define DEFAULT_CACHE_DIRECTORY "~/.cache/xwax/track"

void track_cache_log(const char* format, ...)
{
	gchar* wholeMessage = g_strconcat("Track cache: ", format, "\n", NULL);
	
	va_list ap;
    va_start(ap, format);	
	
	g_vfprintf(stderr, wholeMessage, ap);
	
	va_end(ap);
	g_free(wholeMessage);
}

void track_cache_set_directory(const char* directory)
{
	cache_directory = directory;
		
	track_cache_log("Set cache directory to \'%s\'", cache_directory);
}

static gboolean parse_cache(GOptionContext *context, GOptionGroup *group, gpointer data, GError **error)
{
	if(isempty(data))
		track_cache_set_directory(DEFAULT_CACHE_DIRECTORY);

	track_cache_set_directory(data);
	return TRUE;
}

static GOptionEntry cacheOptions[] =
{
	{ "cache", 'c', G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK, &parse_cache, "Directory to cache decoded audio files", DEFAULT_CACHE_DIRECTORY},
	{ NULL }
};

GOptionGroup* get_cache_option_group()
{
	GOptionGroup* group = g_option_group_new
	(
		"cache",
		"Cache control options",
		"Show cache control help options",
		NULL,
		NULL
	);
	g_option_group_add_entries(group, cacheOptions);
	return group;
}

gboolean track_cache_is_enabled()
{
	return !isempty(cache_directory);
}

const char* track_cache_get_directory()
{
	return cache_directory;
}

struct cached_track
{
	gchar* path;
	gchar* id;
};

struct cache_read
{
	int fd;
	FILE* file;
};

const char* cached_track_get_id(const cached_track* cache)
{
	return cache->id;
}

void cached_track_set_id(const char* path, cached_track* cache)
{
	gchar* localPath = g_strdup(path);
	char* args[] = {"./xwax_hash", "get", localPath, NULL };
	gint status = 0;
	GError* error = NULL;
	gchar* stdout = NULL;
	gchar* stderr = NULL;	

	gboolean success = g_spawn_sync
		(
			NULL, //working_directory
			args,
			NULL, //envp
			G_SPAWN_SEARCH_PATH | G_SPAWN_FILE_AND_ARGV_ZERO, //flags
			NULL,
			NULL,
			&stdout,
			&stderr,
			&status,
			&error
		);	
	
	g_free(localPath);	
	
	if(success && status == 0)
	{
		cache->id = stdout;
		track_cache_log("Successfull retrieved id %s", stdout);
	}
	else
	{
		track_cache_log("Could not retrieve id of file %s", path);
		cache->id = NULL;
	}
}

cached_track* cached_track_init(const char* path)
{
	if(!track_cache_is_enabled())
		return NULL;
	
	cached_track* cache = g_new(cached_track, 1);
	cached_track_set_id(path, cache);
	
	//could not get id
	if(isempty(cached_track_get_id(cache)))
		return NULL;
	
	cache->path = g_build_filename(track_cache_get_directory(), cached_track_get_id(cache), NULL);
	
	return cache;
}

int cached_track_file_open(const cached_track* cache, int flags, int perm)
{	
	return g_open(cache->path, flags, perm);
}

int cached_track_file_open_read(const cached_track* cache)
{
	return cached_track_file_open(cache, O_RDONLY | O_NONBLOCK /*| O_SEQUENTIAL | O_BINARY*/, S_IRUSR);
}

gchar* cached_track_file_get_temp_name(const cached_track* cache)
{
	return g_strconcat(cache->path, cache_file_temp_ending, NULL);
}

int cached_track_file_open_write(const cached_track* cache)
{
	gchar* tempFile = cached_track_file_get_temp_name(cache);
	int fd = g_open(tempFile, O_WRONLY | O_APPEND | O_NONBLOCK /*| O_BINARY */ | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
	g_free(tempFile);
	return fd;
}

void cached_track_file_close(const int fd)
{
	close(fd);
}

void cached_track_file_write_close(const cached_track* cache, const int fd)
{
	cached_track_file_close(fd);
	g_rename(cached_track_file_get_temp_name(cache), cache->path);
}

cache_read* cache_read_init(const cached_track* cache)
{
	cache_read* read = g_new(cache_read, 1);
	read->fd = cached_track_file_open_read(cache);
	
	if(read->fd == -1)
	{
		switch(errno)
		{
			case EACCES:
				fprintf(stderr, "EACCES\n");
				break;
			case EEXIST:
				fprintf(stderr, "EEXIST\n");
				break; 
			case EFAULT:
				fprintf(stderr, "EFAULT\n");
				break;
			case EISDIR:
				fprintf(stderr, "EISDIR\n");
				break; 
			case ELOOP:
				fprintf(stderr, "ELOOP\n");
				break; 
			case EMFILE:
				fprintf(stderr, "EMFILE\n");
				break;
			case ENAMETOOLONG:
				fprintf(stderr, "ENAMETOOLONG\n");
				break;
			case ENFILE:
				fprintf(stderr, "ENFILE\n");
				break; 
			case ENODEV:
				fprintf(stderr, "ENODEV\n");
				break;
			case ENOENT:
				fprintf(stderr, "ENOENT\n");
				break;
			case ENOMEM:
				fprintf(stderr, "ENOMEM\n");
				break;
			case ENOSPC:
				fprintf(stderr, "ENOSPC\n");
				break;
			case ENOTDIR:
				fprintf(stderr, "ENOTDIR\n");
				break;
			case ENXIO:
				fprintf(stderr, "ENXIO\n");
				break;
			case EOVERFLOW:
				fprintf(stderr, "EOVERFLOW\n");
				break;
			case EPERM:
				fprintf(stderr, "EPERM\n");
				break;
			case EROFS:
				fprintf(stderr, "EROFS\n");
				break;
			case ETXTBSY:
				fprintf(stderr, "ETXTBSY\n");
				break;
			case EWOULDBLOCK:
				fprintf(stderr, "EWOULDBLOCK\n");
				break; 
		}
		
		g_free(read);
		return NULL;	
	}
	
	read->file = fdopen(read->fd, "r");

	track_cache_log("Found in cache");	
	
	return read;
}

gboolean cached_track_file_exists(const cached_track* cache)
{
	return g_file_test(cache->path, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR);
}

cache_read* cached_track_read(const cached_track* cache)
{
	if(!track_cache_is_enabled())
		return NULL;
		
	if(!cache)
		return NULL;

	if(!cached_track_file_exists(cache))
		return NULL;	

	return cache_read_init(cache);
}

void cached_track_write(const cached_track* cache, const struct track_t *tr)
{
	if(!track_cache_is_enabled())
		return;
	
	if(!cache)
		return;
		
	if(cached_track_file_exists(cache))
		return;

	int fd = cached_track_file_open_write(cache);

	if(fd < 0)
		return; //TODO: messup here

	FILE* fp = fdopen(fd, "wa");	
	
	long bytesLeft = tr->bytes;

	struct track_block_t* block = NULL;
	unsigned int byteUse;	

	//ATTENTION:
	//Do not rely on tr->blocks here because it
	//does only reflect the overall track memory allocation
	for(guint index = 0; /*index < tr->block->len &&*/ bytesLeft > 0; index++)
	{
		block = track_get_block(tr, index);
		
		byteUse = track_block_get_used_bytes(tr, index);		

		for(guint pcmIndex = 0; pcmIndex < byteUse / TRACK_PCM_DATA_SIZE; pcmIndex++)
		{
			fwrite(&block->pcm[pcmIndex], 1 , TRACK_PCM_DATA_SIZE, fp);
		}

		bytesLeft -= byteUse;
	}

	fflush(fp);

	cached_track_file_write_close(cache, fd);
}

void cached_track_destroy(cached_track* cache)
{
	if(!cache)
		return;
	
	g_free(cache->path);
	g_free(cache->id);
	g_free(cache);
}

int cache_read_get_fd(const cache_read* read)
{
	return read->fd;
}

int cache_read_block(cache_read* read, signed short* pcm, guint pcmCount)
{
	return fread(pcm, TRACK_PCM_DATA_SIZE, pcmCount, read->file) * TRACK_PCM_DATA_SIZE;
}

void cache_read_destroy(cache_read* read)
{
	if(!read)
		return;
		
	cached_track_file_close(read->fd);
	g_free(read);
}

