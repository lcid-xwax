/*
 * Copyright (C) 2008 Mark Hills <mark@pogo.org.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#ifdef WITH_JACK
#include "jack.h"
#endif

#ifdef WITH_ALSA
#include "alsa.h"
#endif

#include "device.h"
#include "interface.h"
#include "library.h"
#include "oss.h"
#include "player.h"
#include "rig.h"
#include "timecoder.h"
#include "track.h"
#include "xwax.h"
#include "track_cache.h"

#define MAX_DECKS 3

#define DEFAULT_IMPORTER "xwax_import"

/* We don't use the full flexibility of a rig, and just have a direct
 * correspondence between a device, track, player and timecoder */

struct deck_t {
    struct device_t device;
    struct track_t track;
    struct player_t player;
    struct timecoder_t timecoder;
};


static void deck_init(struct deck_t *deck, const char *importer)
{
    track_init(&deck->track, importer);
    timecoder_init(&deck->timecoder);
    player_init(&deck->player);
    player_connect_track(&deck->player, &deck->track);    
}


static void deck_clear(struct deck_t *deck)
{
    track_clear(&deck->track);
    timecoder_clear(&deck->timecoder);
    player_clear(&deck->player);
    device_clear(&deck->device);
}


static void connect_deck_to_interface(struct interface_t *iface, int n,
                                      struct deck_t *deck)
{
    iface->timecoder[n] = &deck->timecoder;
    iface->player[n] = &deck->player;
}


static void connect_deck_to_rig(struct rig_t *rig, int n, struct deck_t *deck)
{
    rig->device[n] = &deck->device;
    rig->track[n] = &deck->track;
    rig->player[n] = &deck->player;
    rig->timecoder[n] = &deck->timecoder;
}


void usage(FILE *fd)
{
/*
    fprintf(fd, "Usage: xwax [<parameters>]\n\n"
      "  -l <directory> Directory to scan for audio tracks\n"
      "  -c <directory> Directory to cache decoded audio files\n"
      "  -t <name>      Timecode name\n"
      "  -i <program>   Specify external importer (default '%s')\n"
      "  -h             Display this message\n\n",
      DEFAULT_IMPORTER);

    fprintf(fd, "OSS device options:\n"
      "  -d <device>    Build a deck connected to OSS audio device\n"
      "  -b <n>         Number of buffers (default %d)\n"
      "  -f <n>         Buffer size to request (2^n bytes, default %d)\n\n",
      DEFAULT_OSS_BUFFERS, DEFAULT_OSS_FRAGMENT);

#ifdef WITH_ALSA
    fprintf(fd, "ALSA device options:\n"
      "  -a <device>    Build a deck connected to ALSA audio device\n"
      "  -m <ms>        Buffer time (default %dms)\n\n",
      DEFAULT_ALSA_BUFFER);
#endif

#ifdef WITH_JACK
    fprintf(fd, "JACK options:\n"
      "  -j             Build a jack deck, repeat for multiple decks\n\n");
#endif
*/

    fprintf(fd, "Device options and -i apply to subsequent devices.\n"
      "Decks and audio directories can be specified multiple times.\n\n"
      "Available timecodes (for use with -t):\n"
      "  serato_2a (default), serato_2b, serato_cd, traktor_a, traktor_b\n\n"
      "eg. Standard 2-deck setup\n"
      "  xwax -l ~/music -d /dev/dsp -d /dev/dsp1\n\n"
      "eg. Use a larger buffer on a third deck\n"
      "  xwax -l ~/music -d /dev/dsp -d /dev/dsp1 -f 10 -d /dev/dsp2\n\n");

#ifdef WITH_ALSA
    fprintf(fd, "eg. Use OSS and ALSA devices simultaneously\n"
            "  xwax -l ~/music -d /dev/dsp -a hw:1\n\n");
#endif    
}

static char* libraryPath = NULL;
static char* importer = DEFAULT_IMPORTER;

static GOptionEntry mainOptions[] = 
{
	//Save path to music library
	{ "library", 'l', 0, G_OPTION_ARG_STRING, &libraryPath, "Directory to scan for audio tracks", "~/Music"},
	{ "importer", 'i', 0, G_OPTION_ARG_STRING, &importer, "Specify external importer", DEFAULT_IMPORTER},
	{ NULL }
};

static guint decks = 0;
static struct deck_t deck[MAX_DECKS];
static struct rig_t rig;
static struct interface_t iface;

int main(int argc, char *argv[])
{
    int n;

    struct library_t library;
    struct listing_t listing;
    
    fprintf(stderr, BANNER "\n\n" NOTICE "\n\n");
    
    interface_init(&iface);
    rig_init(&rig);
    library_init(&library);

	GError *error = NULL;
	GOptionContext *context;

	context = g_option_context_new("- virtual dj application (Just a better way to dj)");
	g_option_context_add_main_entries(context, mainOptions, NULL);
#ifdef WITH_ALSA
	g_option_context_add_group(context, get_alsa_option_group());	
#endif
#ifdef WITH_JACK
	g_option_context_add_group(context, get_jack_option_group());
#endif
	g_option_context_add_group(context, get_oss_option_group());

	g_option_context_add_group(context, get_timecode_option_group());

	g_option_context_add_group(context, get_cache_option_group());
	
	if(!g_option_context_parse(context, &argc, &argv, &error))
	{
		g_printerr("Option error: %s\n", error->message);
		return -1;
	}
	
	g_option_context_free(context);

    if(decks == 0) {
        g_printerr("You need to give at least one audio device to use "
                "as a deck; try -h.\n");
        return -1;
    }

    iface.players = decks;
    iface.timecoders = decks;

    fprintf(stderr, "Building timecode lookup tables...\n");
    if(timecoder_build_lookup() == -1)
    {
    	g_printerr("Building timecode lookup tables failed\n"); 
        return -1;
    }    

    /* Connect everything up. Do this after selecting a timecode and
     * built the lookup tables. */

    for(n = 0; n < decks; n++)
        player_connect_timecoder(&deck[n].player, &deck[n].timecoder);

    /* Load in a music library */
    if(libraryPath)
    	library_import(&library, libraryPath);
    
    fprintf(stderr, "Indexing music library...\n");
    listing_init(&listing);
    library_get_listing(&library, &listing);
    listing_sort(&listing);
    iface.listing = &listing;
    
    fprintf(stderr, "Starting threads...\n");
    if(rig_start(&rig) == -1)
        return -1;

    fprintf(stderr, "Entering interface...\n");
    interface_run(&iface);
    
    fprintf(stderr, "Exiting cleanly...\n");

    if(rig_stop(&rig) == -1)
        return -1;
    
    for(n = 0; n < decks; n++)
        deck_clear(&deck[n]);
    
    timecoder_free_lookup();
    listing_clear(&listing);
    library_clear(&library);
    
    fprintf(stderr, "Done.\n");
    
    return 0;
}

guint get_device_count()
{
	return decks;
}

struct device_t* createDevice(GError** error)
{
	// Create a deck
/*
	if (argv[0][1] != 'j') {
		if(argc < 2) {
			fprintf(stderr, "-%c requires a device path as an argument.\n",
				argv[0][1]);
			return -1;
		}
	}
*/
	//disable details for now
	char* details = "";

	if(decks == MAX_DECKS) {
		*error = g_error_new(268, G_OPTION_ERROR_FAILED, "Too many decks (maximum %d); aborting.", MAX_DECKS);		
		return NULL;
	}
            
	fprintf(stderr, "Initialising deck %d (%s)...\n", decks, details);

	deck_init(&deck[decks], importer);

	// Work out which device type we are using, and initialise
	// an appropriate device.

	struct device_t* device = &deck[decks].device;

	decks++;

	return device;
}

gboolean connectAudio(struct device_t* device, int r)
{
	if(r == -1)	
		return FALSE;

	int index = decks - 1;

	// The timecoder and player are driven by requests from
	// the audio device
            
	device_connect_timecoder(device, &deck[index].timecoder);
	device_connect_player(device, &deck[index].player);

	// The rig and interface keep track of everything whilst
	// the program is running

	connect_deck_to_interface(&iface, index, &deck[index]);
	connect_deck_to_rig(&rig, index, &deck[index]);

	return TRUE;
}

