/*
 * Copyright (C) 2008 Mark Hills <mark@pogo.org.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef LIBRARY_H
#define LIBRARY_H

#include <glib.h>

struct record_t;

/* Library acts as a storage of the actual strings */

struct library_t {
	GPtrArray* records;
};

/* Listing just points within a library */

struct listing_t {
	GPtrArray* records;
};

void library_init(struct library_t *li);
void library_clear(struct library_t *li);
void library_add(struct library_t *li, struct record_t *lr);
int library_import(struct library_t *li, char *path);
void library_get_listing(struct library_t *li, struct listing_t *ls);

void listing_init(struct listing_t *ls);
void listing_clear(struct listing_t *ls);
void listing_blank(struct listing_t *ls);
void listing_add(struct listing_t *li, struct record_t *lr);
int listing_match(struct listing_t *src, struct listing_t *dest, char *match);
void listing_debug(struct listing_t *ls);
void listing_sort(struct listing_t *ls);
size_t listing_get_count(const struct listing_t* ls);
guint listing_get_field_size(const struct listing_t* ls, const unsigned int index);
struct record_t* listing_get_record(const struct listing_t* ls, guint index);

#endif
