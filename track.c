/*
 * Copyright (C) 2008 Mark Hills <mark@pogo.org.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/poll.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <math.h>

#include "track.h"
#include "track_cache.h"

#define LOCK(tr) pthread_mutex_lock(&(tr)->mx)
#define UNLOCK(tr) pthread_mutex_unlock(&(tr)->mx)

static guint average_blocks = 0;
static guint block_allocation_count = 0;

static guint get_average_blocks(const guint currentBlockCount)
{
	guint new_block_allocation_count = block_allocation_count + 1;
	average_blocks = ((float)block_allocation_count / (float)new_block_allocation_count * (float)(average_blocks)) + ((float)currentBlockCount / (float)new_block_allocation_count);
	block_allocation_count = new_block_allocation_count;
	return average_blocks; 
}

/* Start the importer process. On completion, pid and fd are set */

static int start_import(struct track_t *tr, const char *path)
{	
	tr->cache = cached_track_init(path);
	
	//try to reserve cache file
	tr->cache_read = cached_track_read(tr->cache); 	

	if(tr->cache_read)
		tr->fd = cache_read_get_fd(tr->cache_read);
	else		
	{
	    int pstdout[2];
	
	    if(pipe(pstdout) == -1) {
	        perror("pipe");
	        return -1;
	    }
	    
	    tr->pid = vfork();
	    
	    if(tr->pid < 0) {
	        perror("vfork");
	        return -1;
	        
	    } else if(tr->pid == 0) { /* child */
	
	        close(pstdout[0]);
	        dup2(pstdout[1], STDOUT_FILENO);
	        close(pstdout[1]);
	        
	        if(execl(tr->importer, "import", path, NULL) == -1) {
	            perror("execl");
	            exit(-1);
	            return 0;
	        }
	    }
	
	    close(pstdout[1]);
	    tr->fd = pstdout[0];
	    if(fcntl(tr->fd, F_SETFL, O_NONBLOCK) == -1) {
	        perror("fcntl");
	        return -1;
	    }
	}

    tr->bytes = 0;
    //tr->length = 0;
    tr->ppm = 0;
    tr->overview = 0;
    tr->eof = 0;
    
    return 0;
}

guint track_get_used_blocks(const struct track_t* tr)
{
	//double cast is necessary here because it
	//otherwise converts the argument to int
	return ceil((double)tr->bytes / BYTES_PER_PCM_BLOCK);
}

guint track_get_last_block_used_bytes(const struct track_t* tr)
{
	return tr->bytes % BYTES_PER_PCM_BLOCK;
}

guint track_block_get_used_bytes(const struct track_t* tr, const guint index)
{
	guint count = track_get_used_blocks(tr);

	if(count < (index + 1))
		return 0;
	else if(count == (index + 1))
		return tr->bytes - (index * BYTES_PER_PCM_BLOCK);
	else
		return BYTES_PER_PCM_BLOCK;
}

void track_blocks_shrink(struct track_t* tr, const guint newSize)
{
	//new size contains an invalid value
	if(newSize >= tr->blocks)
		return;
	
	for(guint index = tr->blocks - 1; index > newSize - 1; index--)
	{    	
		g_slice_free(struct track_block_t, track_get_block(tr, index));
	}
        		
	g_ptr_array_remove_range(tr->block, newSize, tr->blocks - newSize);
	
	fprintf(
		stderr,
		"Deallocated track blocks (freed %d using %zu bytes).\n",
		tr->blocks - newSize,
		(tr->blocks - newSize) * sizeof(struct track_block_t));
	
	tr->blocks = newSize;
}

/* Conclude the importer process. To be called whether the importer
 * was aborted or completed successfully */

static int stop_import(struct track_t *tr)
{		
    int status;

    if(close(tr->fd) == -1) {
        perror("close");
        return -1;
    }    

	if(tr->cache_read)
	{
		cache_read_destroy(tr->cache_read);
		tr->cache_read = NULL;
		cached_track_destroy(tr->cache);
		tr->cache = NULL;
	
		status = 0;
	}
	else
	{
	    if(waitpid(tr->pid, &status, 0) == -1) {
	        perror("waitpid");
	        return -1;
	    }
	}   
    fprintf(stderr, "Track importer exited with status %d.\n", status);

    return 0;
}

guint track_get_sample_count(const struct track_t* tr)
{
	return tr->bytes / BYTES_PER_SAMPLE;
}

/* Read the next block of data from the file. Return -1 when an error
 * occurs and requires our attention, 1 if there is no more data to be
 * read, otherwise zero. */

static int read_from_pipe(struct track_t *tr)
{
    int r, ls;
    size_t m;
    unsigned short v;
    unsigned int s, w;
    struct track_block_t *block;    

    /* Check whether we need to allocate a new block */        
    
    if(tr->bytes >= tr->blocks * BYTES_PER_PCM_BLOCK) {              
        block = g_slice_new(struct track_block_t);

        if(!block) {
            perror("malloc");
            return -1;
        }

		tr->blocks++;
		g_ptr_array_add(tr->block, block);

        fprintf(stderr, "Allocated new track block (%d at %zu bytes).\n",
                tr->blocks, tr->bytes);
    }

    /* Load in audio to the end of the current block. We've just
     * allocated a new one if needed, so no possibility of read()
     * returning zero, except for EOF */

    block = track_get_block(tr, tr->bytes / BYTES_PER_PCM_BLOCK);
         
    if(tr->cache_read)    	
		r = cache_read_block(tr->cache_read, block->pcm, TRACK_BLOCK_PCM_LENGTH);
	else
    {			
		guint used = track_get_last_block_used_bytes(tr);
	       
	    r = read(tr->fd, (char*)block->pcm + used,
	             BYTES_PER_PCM_BLOCK - used);
    }	            	         
	
    if(r == -1) {
        if(errno == EAGAIN)
            return 0;
        perror("read");
        return -1;

    } else if(r == 0) {    	    		
        m = BYTES_PER_PCM_BLOCK * tr->blocks / 1024;
        fprintf(stderr, "Track memory %zuKb PCM, %zuKb PPM, %zuKb overview.\n",
                m, m / TRACK_PPM_RES, m / TRACK_OVERVIEW_RES);

        return 1;
    }
	
	/* Meter the audio which has just been read */
    
	for(s = track_get_sample_count(tr); s < (tr->bytes + r) / BYTES_PER_SAMPLE; s++) {
		ls = s % TRACK_BLOCK_SAMPLES;
        
		v = (abs(block->pcm[ls * TRACK_CHANNELS])
		+ abs(block->pcm[ls * TRACK_CHANNELS]));

		/* PPM-style fast meter approximation */

		if(v > tr->ppm)
			tr->ppm += (v - tr->ppm) >> 3;
		else
			tr->ppm -= (tr->ppm - v) >> 9;
        
		block->ppm[ls / TRACK_PPM_RES] = tr->ppm >> 8;
        
		/* Update the slow-metering overview. Fixed point arithmetic
		 * going on here */

		w = v << 16;
        
		if(w > tr->overview)
			tr->overview += (w - tr->overview) >> 8;
		else
			tr->overview -= (tr->overview - w) >> 17;

		block->overview[ls / TRACK_OVERVIEW_RES] = tr->overview >> 24;
	}
	
	tr->bytes += r;

	return 0;
}


void track_init(struct track_t *tr, const char *importer)
{
    tr->importer = importer;
    tr->pid = 0;

    tr->artist = NULL;
    tr->title = NULL;
    tr->name = NULL;
    tr->cache = NULL;
    tr->cache_read = NULL;

    tr->blocks = 0;
	tr->block = g_ptr_array_new();
    tr->bytes = 0;
    //tr->length = 0; 

    pthread_mutex_init(&tr->mx, 0); /* always returns zero */

    tr->status = TRACK_STATUS_VALID;
}

/* Destroy this track from memory, and any child process */

int track_clear(struct track_t *tr)
{
/*
    int n;
*/

    /* Force a cleanup of whichever state we are in */	
	
    if(tr->status > TRACK_STATUS_VALID) {
        if(tr->status == TRACK_STATUS_IMPORTING) {
            if(!tr->cache_read && kill(tr->pid, SIGKILL) == -1) {
                perror("kill");
                return -1;
            }
        }
        if(stop_import(tr) == -1)
            return -1;
    }

	track_blocks_shrink(tr, 0);	
	
	g_ptr_array_free(tr->block, FALSE);

    if(pthread_mutex_destroy(&tr->mx) == EBUSY) {
        fprintf(stderr, "Track busy on destroy.\n");
        return -1;
    }
    
    cached_track_destroy(tr->cache);
    tr->cache = NULL;

    return 0;
}


/* Return the number of file descriptors which should be watched for
 * this track, and fill pe */

int track_pollfd(struct track_t *tr, struct pollfd *pe)
{
    int r;

    LOCK(tr);

    if(tr->status == TRACK_STATUS_IMPORTING && !tr->eof) {
        pe->fd = tr->fd;
        pe->revents = 0;
        pe->events = POLLIN | POLLHUP | POLLERR;
        tr->pe = pe;
        r = 1;
    } else {
        tr->pe = NULL;
        r = 0;
    }

    UNLOCK(tr);
    return r;
}


/* Handle any activity on this track, whatever the current state */

int track_handle(struct track_t *tr)
{
    int r;

    /* Only one thread is allowed to call this function, and it owns
     * the poll entry */

    if(!tr->pe || !tr->pe->revents)
        return 0;

    LOCK(tr);

    if(tr->status == TRACK_STATUS_IMPORTING && !tr->eof) {
        r = read_from_pipe(tr);
        if(r != 0)
        {
        	if(r > 0)
        		cached_track_write(tr->cache, tr);
        		
        	guint currentUsedBlocks = track_get_used_blocks(tr);        	
        	
        	//currently we have more blocks allocated
        	//than are in use
        	if(currentUsedBlocks < tr->blocks)
        	{        
        		guint averageUsedBlocks = get_average_blocks(track_get_used_blocks(tr));	        	
        		track_blocks_shrink(tr, MAX(averageUsedBlocks, currentUsedBlocks));
        	}       		
        		
            tr->eof = 1;
        }
    }

    UNLOCK(tr);
    return 0;
}


/* A request to begin importing a new track. Can be called when the
 * track is in any state */

int track_import(struct track_t *tr, const char *path)
{	
    int r;

    LOCK(tr);

    /* Abort any running import process */

    if(tr->status != TRACK_STATUS_VALID) {
        if(tr->status == TRACK_STATUS_IMPORTING) {
            if(!tr->cache_read && kill(tr->pid, SIGTERM) == -1) {
                perror("kill");
                UNLOCK(tr);
                return -1;
            }
        }
        if(stop_import(tr) == -1) {
            UNLOCK(tr);
            return -1;
        }
    }

    /* Start the new import process */

    r = start_import(tr, path);
    if(r < 0) {
        UNLOCK(tr);
        return -1;
    }
    tr->status = TRACK_STATUS_IMPORTING;

    UNLOCK(tr);

    rig_awaken(tr->rig);

    return 0;
}


/* Do any waiting for completion of importer process if needed */

int track_wait(struct track_t *tr)
{
    LOCK(tr);

    if(tr->status == TRACK_STATUS_IMPORTING && tr->eof) {
        stop_import(tr);
        tr->status = TRACK_STATUS_VALID;
    }

    UNLOCK(tr);
    return 0;
}
