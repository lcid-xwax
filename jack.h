
#ifndef JACK_H
#define JACK_H

#include <glib.h>
#include "device.h"

GOptionGroup* get_jack_option_group();

int jack_init(struct device_t *dv, int deck_index);

#endif
