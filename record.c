#include <ctype.h>
#include <string.h>
#include <glib.h>
#include <stdarg.h>
#include "record.h"

struct record_t
{
	char* name,
		*pathname;
	
	char** fieldValues;
	
	unsigned int fieldCount;
};

struct record_t* record_init(const char* pathname, ...)
{
	if(!pathname)
		return NULL;

	struct record_t* pRecord = g_new(struct record_t, 1);
	pRecord->pathname = g_strdup(pathname);

	pRecord->name = strrchr(pRecord->pathname, G_DIR_SEPARATOR);
	pRecord->name++;
	
	va_list ap;

	va_start(ap, pathname);
	
	const char* field = NULL;
	
	unsigned int fieldCount  = 0;
	
	while((field = va_arg(ap, const char*)))
	{
		fieldCount++;
	}
	
	va_end(ap);
		
	pRecord->fieldValues = g_new(char*, fieldCount);
	pRecord->fieldCount = fieldCount;
	
	va_start(ap, pathname);		
    
    for(int i = 0; (field = va_arg(ap, const char*)); i++)
	{
		pRecord->fieldValues[i] = g_strdup(field);
	}
	
	va_end(ap);
	
	return pRecord;
}

const char* record_get_field(const struct record_t* pRecord, guint index)
{
	return pRecord->fieldValues[index];
}

unsigned int record_get_field_count(const struct record_t* pRecord)
{
	return pRecord->fieldCount;
}

char* record_get_name(const struct record_t* pRecord)
{
	return pRecord->name;
}

char* record_get_pathname(const struct record_t* pRecord)
{
	return pRecord->pathname;
}

void record_destroy(struct record_t* record)
{
	for(unsigned int i = 0; i < record->fieldCount; i++)
	{
		g_free(record->fieldValues[i]);
	}
	
	g_free(record->fieldValues);	
	g_free(record->pathname);
	g_free(record);
}

