#ifndef TRACK_CACHE_H
#define TRACK_CACHE_H

#include <glib.h>

void track_cache_set_directory(const char* directory);

GOptionGroup* get_cache_option_group();

struct cached_track;
typedef struct cached_track cached_track;
struct cache_read;
typedef struct cache_read cache_read;

cached_track* cached_track_init(const char* path);
cache_read* cached_track_read(const cached_track* cache);
void cached_track_write(const cached_track* cache, const struct track_t *tr);
int cache_read_get_fd(const cache_read* read);
int cache_read_block(cache_read* read, signed short* pcm, guint pcmCount);
void cache_read_destroy(cache_read* read);
void cached_track_destroy(cached_track* cache);

#endif

