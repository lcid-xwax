/*
 * Copyright (C) 2008 Mark Hills <mark@pogo.org.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <ctype.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "library.h"
#include "record.h"

/*
#define BLOCK 256 // number of library entries

#define MIN(a, b) (a<b?a:b)
*/

static int strmatch(char *little, char *big)
{
    size_t n, m, match, little_len;

    little_len = strlen(little);

    for(n = 0; n + little_len <= strlen(big); n++) {
        
        match = 1;

        for(m = 0; m < little_len; m++) {
            if(tolower(little[m]) != tolower(big[n + m])) {
                match = 0;
                break;
            }
        }

        if(match)
            return 1;
    }

    return 0;
}

/* Copy src (ms characters) to dest (length md, including '\0'), and
 * ensure the result is safe and null terminated */

/*
static int strmcpy(char *dest, int md, char *src, int ms)
{
    strncpy(dest, src, MIN(ms, md));
    if(ms < md)
        dest[ms] = '\0';
    else
        dest[md - 1] = '\0';

    return 0;
}
*/

static struct record_t* extract_info(const char* pathname, char *filename)
{
    char *c, *mi, *dt, *end;

    c = filename;
    end = filename;

    while(*c != '\0') {
        end = c;
        c++;
    }

    end++;

    c = filename;
    mi = end;
   
    while(c < end - 3) {
        if(c[0] == ' ' && (c[1] == '-' || c[1] == '_') && c[2] == ' ') {
            mi = c;
            break;
        }

        c++;
    }

    dt = strrchr(filename, '.');

    struct record_t *lr = NULL;
    
    char* artist = filename;
    
    if(mi == end) {
		lr = record_init(pathname, artist, "", NULL);
    } else {
		*mi = '\0';		

		if(dt)
			*dt = '\0';

		char* title = mi + 3;

		lr = record_init(pathname, artist, title, NULL);
    }

    return lr;
}

void library_init(struct library_t *li)
{
	li->records = g_ptr_array_new(); 
}

void clear_record(gpointer data, gpointer user_data)
{
	record_destroy(data);
}

void library_clear(struct library_t *li)
{	
	g_ptr_array_foreach(li->records, clear_record, NULL);

	g_ptr_array_free(li->records, FALSE);
}


void library_add(struct library_t *li, struct record_t *lr)
{	
	g_ptr_array_add(li->records, lr);    
}

int library_import(struct library_t *li, char *path)
{
    DIR *dir;
    struct dirent *de;
    struct record_t* record = NULL;
    struct stat st;

    fprintf(stderr, "Scanning directory '%s'...\n", path);
    
    dir = opendir(path);
    if(!dir) {
        fprintf(stderr, "Failed to scan; aborting.\n");
        return -1;
    }
    
    while((de = readdir(dir)) != NULL) {

        if(de->d_name[0] == '.') // hidden or '.' or '..'
            continue;
        
        char* pathname = g_newa(char, strlen(path) + 1 + strlen(de->d_name) + 1);
        sprintf(pathname, "%s%c%s", path, G_DIR_SEPARATOR, de->d_name);

        if(stat(pathname, &st) == -1) {
            perror("stat");
            continue;
        }

        // If the entry is a directory, recursively import the contents
        // of the directory. Otherwise, assume a file.

        if(S_ISDIR(st.st_mode)) {
            library_import(li, pathname);
       
        } else {
            record = extract_info(pathname, de->d_name);
            library_add(li, record);
        }	
    }
    
    closedir(dir);

    return 0;
}

unsigned int library_get_count(struct library_t* li)
{
	return li->records->len;
}

struct record_t* library_get_record(struct library_t* li, const guint index)
{	
	return g_ptr_array_index(li->records, index);
}

void library_get_listing(struct library_t *li, struct listing_t *ls)
{
    int n;

    for(n = 0; n < library_get_count(li); n++)
        listing_add(ls, library_get_record(li, n));
}


void listing_init(struct listing_t *ls)
{
    ls->records = g_ptr_array_new();
}


void listing_clear(struct listing_t *ls)
{
	g_ptr_array_free(ls->records, FALSE);
}


void listing_blank(struct listing_t *ls)
{
	if(ls->records->len != 0)
		g_ptr_array_remove_range(ls->records, 0, listing_get_count(ls) - 1);
}

size_t listing_get_count(const struct listing_t* ls)
{
	return ls->records->len;
}

guint listing_get_field_size(const struct listing_t* ls, const unsigned int index)
{
	return 200;
}

void listing_add(struct listing_t *ls, struct record_t *lr)
{
	g_ptr_array_add(ls->records, lr);
}

struct record_t* listing_get_record(const struct listing_t* ls, guint index)
{
	return g_ptr_array_index(ls->records, index);
}

gint compare_records(gconstpointer a, gconstpointer b)
{
	return strcmp(record_get_name(a), record_get_name(b));
}

void listing_sort(struct listing_t *ls)
{
	g_ptr_array_sort(ls->records, compare_records);
}

int listing_match(struct listing_t *src, struct listing_t *dest, char *match)
{
    int n;
    struct record_t *re;
 
    fprintf(stderr, "Matching '%s'\n", match);
    
    for(n = 0; n < listing_get_count(src); n++) {
        re = listing_get_record(src, n);
        
        if(strmatch(match, record_get_name(re)))
            listing_add(dest, re);
    }

    return 0;
}

void listing_debug(struct listing_t *ls)
{
    int n;

	struct record_t* record = NULL;
	
	for(n = 0; (record = listing_get_record(ls, n)); n++)
	{
		fprintf(stderr, "%d: %s\n", n, record_get_name(record));
	}
}

