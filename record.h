struct record_t;

//functions to access record
struct record_t* record_init(const char* pathname, ...);
char* record_get_artist(const struct record_t* pRecord);
char* record_get_title(const struct record_t* pRecord);
char* record_get_name(const struct record_t* pRecord);
char* record_get_pathname(const struct record_t* pRecord);
const char* record_get_field(const struct record_t* pRecord, unsigned int index);
unsigned int record_get_field_count(const struct record_t* pRecord);
void record_destroy(struct record_t* record);
