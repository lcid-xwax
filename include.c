#include <glib.h>
#include <string.h>
#include "include.h"

gboolean file_exists(const char* file)
{
	return g_file_test(file, G_FILE_TEST_IS_REGULAR);
}

int isempty(const char* string)
{
	return !string || (strlen(string) == 0);
};

